#!/bin/bash 
IMAGE_NAME_MYSQL="mysql"
IMAGE_NAME_PHP_FPM="php-fpm"
IMAGE_NAME_NGINX="nginx"
IMAGE_NAME_NODE="node"
IMAGE_NAME_PHP_MY_ADMIN="phpmyadmin"

# create network
docker network create frontend
docker network create backend

#build mysql
docker build -t ${IMAGE_NAME_MYSQL} -f evironments/mysql/Dockerfile .

docker run -itd --name=${IMAGE_NAME_MYSQL} \
    --network  backend\
    -p 3306:3306 \
    -v $(pwd -W)/evironments/mysql/data:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=root \
    ${IMAGE_NAME_MYSQL}

# build php-fpm
docker build -t ${IMAGE_NAME_PHP_FPM} -f evironments/php-fpm/Dockerfile .

docker run -itd --name=${IMAGE_NAME_PHP_FPM} \
    --network  backend\
    -p 9000:9000 \
    -v $(pwd -W)/source/api:/var/www/api  \
    ${IMAGE_NAME_PHP_FPM}

#build phpmyadmin
docker build -t ${IMAGE_NAME_PHP_MY_ADMIN} -f evironments/phpmyadmin/Dockerfile .

docker run -itd --name=${IMAGE_NAME_PHP_MY_ADMIN} \
    -link ${IMAGE_NAME_MYSQL} \
    --network  backend\
    -p 8081:8081 \
    -v $(pwd -W)/evironments/mysql/data:/var/lib/mysql \
    -e PMA_ARBITRARY=1 \
    -e PMA_HOST=mysql \
    ${IMAGE_NAME_PHP_MY_ADMIN}

# build nginx
docker build -t ${IMAGE_NAME_NGINX} -f evironments/nginx/Dockerfile .

docker run -itd --name=${IMAGE_NAME_NGINX} \
 --network  backend\
 -p 8080:80 \
 -v $(pwd -W)/source:/var/www \
 -v $(pwd -W)/evironments/nginx/sites:/etc/nginx/sites-available \
 ${IMAGE_NAME_NGINX}

 # build node
 docker build -t ${IMAGE_NAME_NODE} -f evironments/node/Dockerfile .

docker run -itd --name=${IMAGE_NAME_NODE} \
 --network  backend\
 -p 3000:3000 \
 -v $(pwd -W)/source/client:/var/www/client \
 ${IMAGE_NAME_NODE}